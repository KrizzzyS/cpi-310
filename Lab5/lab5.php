<?php
// Function to calculate BMI, assuming person is 5 feet, 60 inches
function calculateBodyMassIndex($weight, $height = 60) {
	return $weight / ($height*$height)*703;
}

print("<center width = 50%>");
print("<h1> BMI Table</h1>");
print("<table border = 2 width = 50%>");
print("<tr>");
print("<td align = center> Weight </td>");
print("<td align = center> BMI (at 5 Feet) </td>");
print("</tr>");

for($i = 120; $i <= 250; $i+=5) {
	print("<tr>");
	print("<td> $i </td>");
	printf("<td> %.2d" . " </td>", calculateBodyMassIndex($i));
}

print("</table>");
print("</center>");
?>