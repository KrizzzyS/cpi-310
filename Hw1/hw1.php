<?php
define("TIRE_PRICE", 99.99);
define("FRONT_BRAKE_PRICE", 49.99);
define("ENGINE_OIL_PRICE", 9.99);
define("BRAKE_OIL_PRICE", 19.99);

$numTires = $_POST['tires'];
$numFrontBrakes = $_POST['brake'];
$numEngineOils = $_POST['eOil'];
$numBrakeOils = $_POST['bOil'];
$billingAddress = $_POST['billAddress'];
$email = $_POST['email'];

function validateEmailAddress($pEmail) {
	if(filter_var($pEmail, FILTER_VALIDATE_EMAIL)) {
		return true;
	}
	else {
		echo 'Please enter a valid email address.';
		return false;
	}
}

function validateQuanties($tires, $fBrakes, $eOils, $bOils) {
	$optionsTire = array(
		'options' => array(
						  'min_range' => 0,
						  'max_range' => 4,
		 				  )
	);

	$optionsBrake = array(
		'options' => array(
						  'min_range' => 0,
						  'max_range' => 5,
		 				  )
	);

	$optionsEngineOil = array(
		'options' => array(
						  'min_range' => 0,
						  'max_range' => 10,
		 				  )
	);

	$optionsBrakeOil = array(
		'options' => array(
						  'min_range' => 0,
						  'max_range' => 10,
		 				  )
	);
	if(filter_var($tires, FILTER_VALIDATE_INT, $optionsTire) === FALSE || filter_var($fBrakes, FILTER_VALIDATE_INT, $optionsBrake) === FALSE || filter_var($eOils, FILTER_VALIDATE_INT, $optionsEngineOil) === FALSE || filter_var($bOils, FILTER_VALIDATE_INT, $optionsBrakeOil) === FALSE) {
		echo "Number of tires should be less than or equal to 4" .
			 "<br>" .
			 "Front Brakes should be less than or equal to 5" .
			 "<br>" .
			 "Engine and Brake Oil should be less than or equal to 10" .
			 "<br>";
		return false;
	}
	else {
		return true;
	}
}

function calculateTireCost($tires) {
	return number_format(round($tires*TIRE_PRICE, 2), 2);
}

function calculateFrontBrakeCost($fBrakes) {
	return number_format(round($fBrakes*FRONT_BRAKE_PRICE, 2), 2);
}

function calculateEngineOilCost($eOils) {
	return number_format(round($eOils*ENGINE_OIL_PRICE, 2), 2);
}

function calculateBrakeOilCost($bOils) {
	return number_format(round($bOils*BRAKE_OIL_PRICE, 2), 2);
}

function calculateTotalCost($tireCost, $brakeCost, $engineOilCost, $brakeOilCost) {
	return number_format(round($tireCost + $brakeCost + $engineOilCost + $brakeOilCost, 2), 2);
}

$valueFlag = validateQuanties($numTires, $numFrontBrakes, $numEngineOils, $numBrakeOils);
$emailFlag = validateEmailAddress($email);

if($valueFlag && $emailFlag) {
	$tireCost = calculateTireCost($numTires);
	$brakeCost = calculateFrontBrakeCost($numFrontBrakes);
	$engineOilCost = calculateEngineOilCost($numEngineOils);
	$brakeOilCost = calculateBrakeOilCost($numBrakeOils);
	$totalCost = calculateTotalCost($tireCost, $brakeCost, $engineOilCost, $brakeOilCost);

	echo '<h1 align = center>Bill Summary</h1>';
	printf('Tires = $%.2f', $tireCost);
	echo '<br>';
	printf('Front Brake = $%.2f', $brakeCost);
	echo '<br>';
	printf('Engine Oil = $%.2f', $engineOilCost);
	echo '<br>';
	printf('Brake Oil = $%.2f', $brakeOilCost);
	echo '<br>';
	printf('TOTAL = $%.2f', $totalCost);
	echo '<br>';
	echo 'Your order has been shipped to';
	echo '<br>';
	echo $billingAddress;
	echo '<br>';

	// subject
	$subject = 'Your Billing Information!';

	// message
	$message = 'Tires = $' . $tireCost . '<br>' . 
			   'Front Brake = $' . $brakeCost . '<br>' .
			   'Engine Oil = $' . $engineOilCost . '<br>' .
			   'Brake Oil = $' . $brakeOilCost . '<br>' . 
			   'Your billing total is $' . $totalCost;

	// To send HTML mail, the Content-type header must be set
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

	// Additional headers
	$headers .= 'From: CPI 301 <cpi310@example.com>' . "\r\n";
	//. is here for string concatenation


	// Mail it
	mail($email, $subject, $message, $headers);
	print ("<p> email has been sent to $email </p>");
}
?>
